<?php

namespace Gallery\Remove;

use App\Models\Admin\Menu;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RemovePackage
{
    private $pathMigration;
    private $pathScript;
    private $pathVues;
    public function __construct()
    {
        $this->pathMigration = database_path('migrations');
        $this->pathScript = public_path('assets/js/admin/gallery');
        $this->pathVues = resource_path('views/vendor/gallery');
    }

    public function run($settings,$migration = false, $script = false, $vies = false)
    {
        chdir(base_path());
        if($migration)
        {
            $categories = $this->pathMigration."/2023_06_30_173338_create_gallery_categories_table.php";
            $images = $this->pathMigration."/2023_06_30_173354_create_gallery_images_table.php";
            $imagesHasCategories = $this->pathMigration."/2023_03_08_071047_create_categories_has_news_table.php";
            $settingsCategories = $this->pathMigration."/2023_08_08_132926_create_gallery_settings_table.php";
            if (File::exists($categories)) {
                exec($settings->php.'\php.exe artisan migrate:rollback --path=database/migrations/2023_06_30_173338_create_gallery_categories_table.php');
                unlink($categories);
            }
            if (File::exists($images)) {

                exec($settings->php.'\php.exe artisan migrate:rollback --path=database/migrations/2023_06_30_173354_create_gallery_images_table.php');
                unlink($images);
            }
            if (File::exists($imagesHasCategories)) {
                exec($settings->php.'\php.exe artisan migrate:rollback --path=database/migrations/2023_06_30_173418_create_gallery_categories_has_gallery_images_table.php');
                unlink($imagesHasCategories);
            }
            if (File::exists($settingsCategories)) {
                exec($settings->php.'\php.exe artisan migrate:rollback --path=database/migrations/2023_08_08_132926_create_gallery_settings_table.php');
                unlink($settingsCategories);
            }

            $menu = Menu::query()->find(999);
            $menu->delete();
        }

        if($script)
        {
            if (File::isDirectory($this->pathScript))
                File::deleteDirectory($this->pathScript);
        }

        if ($vies)
        {
            if (File::isDirectory($this->pathVues))
                File::deleteDirectory($this->pathVues);
        }
    }
}

