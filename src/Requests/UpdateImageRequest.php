<?php

namespace Gallery\Requests;

use Gallery\Models\GalleryCategory;
use Gallery\Models\GalleryImage;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class UpdateImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable',
            'description' => 'nullable',
            'image_url' => 'nullable'
        ];
    }

    public function prepareForValidation()
    {
        $disk = Storage::disk('public');
        $files = $this->file('images');
        if ($files) {
            $fileNames = [];
            $image = GalleryImage::query()->find($this->input('id'));
            if($image) {
                $path = explode("storage", $image->image_url);
                if (count($path) > 1)
                    $disk->delete($path[1]);
            }
            foreach ($files as $file) {
                $image = new \Imagick($file->getRealPath());

                // Установка формата изображения в WebP
                $image->setImageFormat('webp');

                // Установка качества сжатия
                $image->setImageCompressionQuality(70);

                $width = $image->getImageWidth();
                $height = $image->getImageHeight();
                if ($height > 1200) {
                    // Сохраняем пропорции при изменении размера
                    $newHeight = 1200;
                    $newWidth = ($width / $height) * $newHeight;

                    // Изменяем размер изображения
                    $image->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 1);
                }

                $folderName = 'gallery/images/';
                if (!$disk->exists($folderName)) {
                    $disk->makeDirectory($folderName);
                }


                $fileName = $folderName . uniqid() . '.webp';

                $disk->put($fileName, $image->getImageBlob());

                // Освобождение ресурсов
                $image->clear();
                $image->destroy();
                $fileNames[] = "/storage/".$fileName;
            }

            $this->merge([
                'image_url' => $fileNames[0]
            ]);
        }
    }
}
