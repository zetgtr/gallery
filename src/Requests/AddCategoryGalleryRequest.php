<?php

namespace Gallery\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class AddCategoryGalleryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return config('gallery.requests.category_gallery');
    }


    public function prepareForValidation()
    {
        $disk = Storage::disk('public');
        if ($this->file('image')) {
            $file = $this->file('image');

            // Создание экземпляра Imagick изображения
            $image = new \Imagick($file->getRealPath());

            // Установка формата изображения в WebP
            $image->setImageFormat('webp');

            // Установка качества сжатия
            $image->setImageCompressionQuality(70);

            $width = $image->getImageWidth();
            $height = $image->getImageHeight();
            if ($height > 1200) {
                // Сохраняем пропорции при изменении размера
                $newHeight = 1200;
                $newWidth = ($width / $height) * $newHeight;

                // Изменяем размер изображения
                $image->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 1);
            }

            $folderName = 'gallery/category/';
            if (!$disk->exists($folderName)) {
                $disk->makeDirectory($folderName);
            }

            // Генерация уникального имени файла
            $fileName = $folderName . uniqid() . '.webp';

            // Сохранение изображения в указанном формате и диске
            $image->writeImage(public_path('storage/' . $fileName));

            // Освобождение ресурсов
            $image->clear();
            $image->destroy();
            $this->merge([
                'image_url' => "/storage/".$fileName
            ]);
        }
    }
}
