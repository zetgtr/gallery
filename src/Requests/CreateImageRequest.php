<?php

namespace Gallery\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;

class CreateImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'nullable',
            'description' => 'nullable',
            'imagesData' => 'required|array',
            'images' => 'required|array',
            'images.*' => 'image'
        ];
    }

    public function prepareForValidation()
    {
        $disk = Storage::disk('public');
        $files = $this->file('images');

        if ($files) {
            $fileNames = [];
            foreach ($files as $file) {
                $image = new \Imagick($file->getRealPath());


                $image->setImageFormat('webp');

                // Установка качества сжатия
                $image->setImageCompressionQuality(70);

                $width = $image->getImageWidth();
                $height = $image->getImageHeight();
                if ($height > 1200) {
                    // Сохраняем пропорции при изменении размера
                    $newHeight = 1200;
                    $newWidth = ($width / $height) * $newHeight;

                    // Изменяем размер изображения
                    $image->resizeImage($newWidth, $newHeight, \Imagick::FILTER_LANCZOS, 1);
                }

                $folderName = 'gallery/images/';
                if (!$disk->exists($folderName)) {
                    $disk->makeDirectory($folderName);
                }


                $fileName = $folderName . uniqid() . '.webp';


                $disk->put($fileName, $image->getImageBlob());

                // Освобождение ресурсов
                $image->clear();
                $image->destroy();

                $fileNames[] = "/storage/".$fileName;
            }
            $this->merge([
                'imagesData' => $fileNames
            ]);
        }
    }

}
