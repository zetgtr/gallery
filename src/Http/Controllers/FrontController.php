<?php

namespace Gallery\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin\PhotoGallery\GalleryCategory;
use Catalog\QueryBuilder\CatalogBuilder;
use Gallery\Models\GallerySettings;
use Gallery\QueryBuilder\GalleryCategoryBuilder;

class FrontController extends Controller
{
    public function index(GalleryCategoryBuilder $categoryBuilder)
    {
        $settings = GallerySettings::query()->find(1);
        return \view('gallery::gallery.front.index',[
            'breadcrumbs' => [...config('gallery.routes'),['title'=>$settings->title,'route'=>'gallery']],
             'seo_title' => $settings->seo_title,
             'title' => $settings->title,
             'description' => $settings->description,
             'seo_description' => $settings->seo_description,
             'seo_keywords' => $settings->seo_keywords,
            'albums' => $categoryBuilder->getPagination(),
        ]);
    }

    public function show($categoryId,GalleryCategoryBuilder $categoryBuilder)
    {
        $settings = GallerySettings::query()->find(1);
        $album = GalleryCategory::find($categoryId);
        return \view('gallery::gallery.front.album',[
            'breadcrumbs' => [...config('gallery.routes'),['title'=>$settings->title,'route'=>'gallery'],['title'=>$album->title,'route'=>$settings->url]],
             'seo_title' => $album->title,
             'title' => $album->title,
             'seo_description' => $settings->seo_description,
             'seo_keywords' => $settings->seo_keywords,
            'album' => $album,
            'photos' => GalleryCategory::find($categoryId)->list()->orderBy(config('gallery.order'),config('gallery.order_position'))->paginate($settings->paginate),
        ]);
    }
}
