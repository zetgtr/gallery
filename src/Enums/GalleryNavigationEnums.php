<?php

namespace Gallery\Enums;

enum GalleryNavigationEnums: string
{
    case GALLERY = "gallery";
    case SETTINGS = "settings";
}
