<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class GalleryCategory extends Model
{
    use HasFactory;
    protected $fillable = [];
    protected $primaryKey = 'id';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('gallery.fillable.category_gallery');
    }

    public function list(): BelongsToMany
    {
        return $this->belongsToMany(GalleryImage::class, 'gallery_categories_has_gallery_images',
            'category_id','image_id', 'id', 'id');
    }
}
