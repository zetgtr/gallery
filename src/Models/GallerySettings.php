<?php

namespace Gallery\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class GallerySettings extends Model
{
    use HasFactory;

    protected $fillable = ['id','url','seo_title','paginate','seo_description','seo_keywords','title','description'];
}
