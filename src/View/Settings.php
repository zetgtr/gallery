<?php

namespace Gallery\View;

use Gallery\Models\GallerySettings;
use Illuminate\View\Component;

class Settings extends Component
{
    public function __construct()
    {
        $this->settings = GallerySettings::query()->find(1);
    }

    public function render()
    {
        return view('gallery::components.settings',['settings'=>$this->settings]);
    }
}
