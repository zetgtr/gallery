<?php

namespace Gallery\QueryBuilder;

use App\Models\Admin\PhotoGallery\GalleryCategory;
use App\QueryBuilder\QueryBuilder;
use Gallery\Enums\GalleryNavigationEnums;
use Gallery\Models\GallerySettings;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;

class GalleryCategoryBuilder extends QueryBuilder
{
    public function __construct()
    {
        $this->model = GalleryCategory::query();
    }

    public function delete($id)
    {
        $category = $this->model->find($id);
        $disc = Storage::disk('public');
        $path = explode("storage",$category->image_url);
        $images = $category->list()->get();

        $imageBuilder = new GalleryImageBuilder();
        foreach ($images as $image)
            $imageBuilder->delete($image->id);

        if(count($path)>1)
            $disc->delete($path[1]);
        $category->delete();
    }

    public function getLinks($key = false)
    {
        $links = [
            GalleryNavigationEnums::GALLERY->value => ['url'=> GalleryNavigationEnums::GALLERY->value, 'name' => 'Альбомы'],
            GalleryNavigationEnums::SETTINGS->value => ['url'=> GalleryNavigationEnums::SETTINGS->value,  'name' => 'Настройки']
        ];

        if($key) $links[$key]['active'] = true;

        return $links;
    }

    public function getAll(): Collection
    {
        return $this->model->orderBy(config('gallery.order'),config('gallery.order_position'))->get();
    }

    public function getPagination()
    {
        $settings = GallerySettings::query()->find(1);
        return $this->model->orderBy(config('gallery.order'),config('gallery.order_position'))->paginate($settings->paginate);
    }
}
