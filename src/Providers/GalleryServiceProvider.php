<?php

namespace Gallery\Providers;

use App\Actions\Fortify\CreateNewUser;
use App\Actions\Fortify\ResetUserPassword;
use App\Actions\Fortify\UpdateUserPassword;
use App\Actions\Fortify\UpdateUserProfileInformation;
use Gallery\QueryBuilder\GalleryCategoryBuilder;
use Gallery\View\Category;
use Gallery\View\Images;
use Gallery\View\Settings;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Illuminate\View\Middleware\ShareErrorsFromSession;

class GalleryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        }

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/gallery'),
        ], 'views');
        $this->publishes([
            __DIR__.'/../../resources/js' => public_path('assets/js/admin/gallery'),
        ], 'script');
        $this->publishes([
            __DIR__.'/../../config/gallery.php' => config_path('gallery.php'),
        ], 'config_gallery');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'gallery');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'gallery');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(Images::class, 'gallery::images');
        Blade::component(Category::class, 'gallery::category');
        Blade::component(Settings::class, 'gallery::settings');
    }

    private function singletons()
    {
        $this->app->singleton(Images::class, function ($app, $parameters) {
            $categoryId = $parameters['categoryId'];
            return new Images($categoryId);
        });
        $this->app->singleton(Category::class, function ($app) {
            $categoryBuilder = $app->make(GalleryCategoryBuilder::class);
            return new Category($categoryBuilder);
        });
        $this->app->singleton(Settings::class, function ($app) {
            return new Settings();
        });

    }
    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryBuilder::class, GalleryCategoryBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/gallery.php', 'gallery');
        $this->singletons();
    }
}
