@extends('layouts.inner')
@section('title', $seo_title)
@section('description', $seo_description)
@section('keywords', $seo_keywords)
@section('page', true)
@section('content')
    <div class="content">
        <x-front.elements.breadcrumbs :breadcrumbs="$breadcrumbs" />
        <div class="container">
            <h1>{{ $title }}</h1>
            <div style="margin-bottom: 50px;" class="objects--inner__desc">{{ $description }}</div>
        </div>
        <section class="objects">
            <div class="container objects__container">

                @foreach ($albums as $album)
                    <x-gallery::front.albums :album="$album" />
                @endforeach
            </div>
        </section>
        <section class="pagination__container">
            {{ $albums->links('pagination::bootstrap-4') }}
        </section>
    </div>
@endsection
