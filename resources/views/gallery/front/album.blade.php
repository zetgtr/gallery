@extends('layouts.inner')
@section('title',$seo_title)
@section('description',$seo_description)
@section('keywords',$seo_keywords)
@section('page',true)
@section('content')
<div class="content">
    <div class="catalog__container">
        <div class="container">
            <x-front.breadcrumbs :breadcrumbs="$breadcrumbs" />
            <h1>{{ $album->title }}</h1>
            <div class="row gphoto">
                @foreach ($photos as $photo)
                <x-gallery::front.album_item :photo="$photo" :album="$album"/>
                @endforeach
            </div>
            <div class="pagination-catalog">
                {{ $photos->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
