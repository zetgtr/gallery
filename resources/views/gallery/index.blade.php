@extends('layouts.admin')
@section('title', 'Фотогалерея')
@section('content')
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.umd.js"></script>
    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@5.0/dist/fancybox/fancybox.css"
    />
    <div class="card">
        <div class="card">
            <x-admin.navigatin-js :links="$links" />
            <div class="card-body">
                <div class="tab-content">
                    <x-gallery::gallery />
                    <x-gallery::settings />
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('assets/plugins/gallery/lightgallery.js') }}"></script>
    <script src="{{ asset('assets/plugins/gallery/lightgallery-1.js') }}"></script>
    <script src="{{ asset('assets/js/admin/utils/files.js') }}"></script>
    <script >
        Fancybox.bind("[data-fancybox]", {
            // Your custom options
        });
        filesClass.galleryAdmin()
    </script>
@endsection
@section("breadcrumb")
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Фотогалерея</li>
        </ol>
    </div>
@endsection
