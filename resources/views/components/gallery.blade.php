<div class="tab-pane fade active show" id="gallery">
    <x-warning />
    <div class="row">
        <div class="col-lg-8">
            <x-gallery::category />
        </div>
        <div class="col-lg-4">
            <h5 class="mb-4 category_form_title"><i class="fas fa-plus-circle" style="color: var(--primary-site);"></i>
                Добавить альбом</h5>
            <form action="{{ route('admin.photo_gallery.store') }}" id="photo_gallery_form" method="POST"
                enctype="multipart/form-data">
                @csrf
                <div id="method"></div>
                <input type="hidden" name="id" id="id_edit" value="">
                <div class="form-group">
                    <label for="title">Заголовок альбома</label>
                    <input class="form-control " id="title" value="" name="title" type="text">
                </div>
                <div class="form-group">
                    <label for="description">Описание альбома</label>
                    <textarea class="form-control " id="description" name="description" type="text"></textarea>
                </div>
                <div class="form-group">
                    <label class="custom-switch ps-0">
                        <input type="checkbox" id="order" class="custom-switch-input">
                        <span class="custom-switch-indicator me-3"></span>
                        <span class="custom-switch-description mg-l-10">Переместить альбом</span>
                    </label>
                </div>
                <div class="form-group">
                    <label for="image_url">Фото альбома</label>
                    <input type="file" class="filepond " name="image">
                </div>
                <div class="pb-0 mt-3">
                    <div class="files-unstyled">
                    </div>
                </div>
                <button type="submit" name="save" class="btn btn-sm btn-success">Сохранить</button>
                <button type="submit" name="save" class="btn btn-sm btn-danger edit_close d-none">Отмена</button>
            </form>
        </div>
    </div>
</div>
