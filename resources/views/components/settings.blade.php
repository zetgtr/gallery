<div class="tab-pane fade w-100" id="settings">
    <form action="{{ route('admin.photo_gallery.settings') }}" class="row" method="post">
        @csrf
        <div class="d-flex flex-column">

            <div class="col-lg-6">
                <div class="form-group">
                    <label for="title">Заголовок</label>
                    <input class="form-control @error('title') is-invalid @enderror" id="title"
                        value="{{ old('title', $settings ? $settings->title : '') }}" name="title" type="text">
                    <x-error errorValue="title" />
                </div>
            </div>
            <div class="form-group col-lg-6">
                <label for="paginate">Пагинация</label>
                <input type="text" name="paginate" id="paginate"
                    class="form-control @error('paginate') is-invalid @enderror"
                    value="{{ old('paginate', $settings ? $settings->paginate : '') }}">
                <x-error errorValue="paginate" />
            </div>
            <div class="form-group col-lg-6">
                <label for="url">Url</label>
                <input class="form-control @error('url') is-invalid @enderror" id="url"
                    value="{{ old('url', $settings ? $settings->url : '') }}" name="url" type="text">
                <x-error errorValue="url" />
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <label for="seo_title">SEO Title</label>
                    <input class="form-control @error('seo_title') is-invalid @enderror" id="seo_title"
                        value="{{ old('seo_title', $settings ? $settings->seo_title : '') }}" name="seo_title"
                        type="text">
                    <x-error errorValue="seo_title" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="seo_keywords">SEO Keywords</label>
                    <input class="form-control @error('seo_keywords') is-invalid @enderror" id="seo_keywords"
                        value="{{ old('seo_keywords', $settings ? $settings->seo_keywords : '') }}" name="seo_keywords"
                        type="text">
                    <x-error errorValue="seo_keywords" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="seo_description">SEO Description</label>
                    <textarea class="form-control @error('seo_description') is-invalid @enderror" id="seo_description"
                        name="seo_description" type="text">{{ old('seo_description', $settings ? $settings->seo_description : '') }}</textarea>
                    <x-error errorValue="seo_description" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="seo_description">Описание</label>
                    <textarea class="form-control @error('description') is-invalid @enderror" id="description"
                              name="description" type="text">{{ old('seo_description', $settings ? $settings->description : '') }}</textarea>
                    <x-error errorValue="description" />
                </div>
            </div>
        </div>

        <div>
            <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
        </div>
    </form>
</div>
