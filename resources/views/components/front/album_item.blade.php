<a class="wow fadeInUp gphoto__item col-sm-6 col-lg-4" href="{{ $photo->image_url }}" data-fancybox="gallery" data-options="{&quot;baseClass&quot;:&quot;gphoto-fancy&quot;}">
    <figure class="image image_alive gphoto__image">
        <img src="{{ $photo->image_url }}" alt="office">
    </figure>
</a>
