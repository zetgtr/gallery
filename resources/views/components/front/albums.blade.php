<a href="{{ route('gallery_show', ['categoryId' => $album->id]) }}" class="gphoto__item col-lg-4 col-sm-6  wow fadeInUp">
    <figure class="image image_alive gphoto__image gphoto__image_shadow">
        <img src="{{ $album->image_url }}" alt="office">
        <figcaption>
            <span class="">{{ $album->title }}</span>
        </figcaption>
    </figure>
</a>
