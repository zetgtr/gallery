<?php

use Gallery\Http\Controllers\FrontController;
use Gallery\Models\GallerySettings;
use Illuminate\Support\Facades\Route;
use \Gallery\Http\Controllers\PhotoGalleryController;


Route::middleware('web')->group(function (){
    Route::middleware('is_admin')->group(function () {
        Route::group(['prefix' => 'admin', 'as' => 'admin.'], static function() {
            Route::resource('photo_gallery',PhotoGalleryController::class);

            Route::group(['prefix' => 'photo_gallery', 'as' => 'photo_gallery.'], static function() {
                Route::post('settings',[PhotoGalleryController::class ,'settings'])->name('settings');
                Route::post('image_store/{category}', [PhotoGalleryController::class, 'storeImage'])->name('image_store');
                Route::post('image_edit/{image}', [PhotoGalleryController::class, 'updateImage'])->name('image_edit');
                Route::delete('image_delete/{image}', [PhotoGalleryController::class, 'destroyImage'])->name('image_delete');
            });
        });
    });
    try {
        $settings = GallerySettings::query()->find(1);
        $url = 'examples';
        if ($settings)
            $url = $settings->url;
        Route::get('/'.$url,[FrontController::class,'index'])->name('gallery');
        Route::get('/'.$url.'/{categoryId}',[FrontController::class,'show'])->name('gallery_show');
    }catch (Exception $exception)
    {

    }
});
