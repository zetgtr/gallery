<?php

use Gallery\Http\Controllers\PhotoGalleryController;
use Illuminate\Support\Facades\Route;



Route::middleware('api')->group(function (){
    Route::group(['prefix'=>"api"],static function(){
        Route::group(['prefix' => 'admin', 'as' => 'admin.'], static function() {
            Route::post('get_gallery_category',[PhotoGalleryController::class,'getCategory']);
            Route::post('get_gallery_image',[PhotoGalleryController::class,'getImage']);
            Route::post('gallery/set_order_category',[PhotoGalleryController::class,'setOrderCategory']);
            Route::post('gallery/set_order_image',[PhotoGalleryController::class,'setOrderImage']);
        });
    });
});
