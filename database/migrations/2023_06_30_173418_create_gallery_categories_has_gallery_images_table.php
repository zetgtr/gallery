<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('gallery_categories_has_gallery_images', function (Blueprint $table) {
            $table->id();
            $table
                ->foreignId('category_id')
                ->references('id')->on('gallery_categories')
                ->cascadeOnDelete();
            $table->foreignId('image_id')
                ->references('id')->on('gallery_images')
                ->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('gallery_categories_has_gallery_images');
    }
};
