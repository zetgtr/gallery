<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('gallery_settings', function (Blueprint $table) {
            $table->string('description')->after('seo_keywords'); // Указываем позицию столбца
        });
    }

    public function down(): void
    {
        Schema::table('gallery_settings', function (Blueprint $table) {
            $table->dropColumn('description');
        });
    }
};
