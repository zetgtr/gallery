<?php

namespace Gallery\Seeders;
use Illuminate\Database\Seeder;
class MenuSeeder extends Seeder
{
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }
    public function getData()
    {
        return [
            ['id' => 999, 'name' => 'Фотогалерея', 'position' => 'left', 'logo' => 'fal fa-image', 'controller' => 'Gallery\Http\Controllers\PhotoGalleryController', 'url' => 'photo_gallery', 'parent' => 5, "controller_type" => null, 'order' => 2]
        ];
    }
}
