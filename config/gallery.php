<?php

use Laravel\Fortify\Features;

return [
    'guard' => 'web',
    'middleware' => ['web'],
    'auth_middleware' => 'auth',
    'passwords' => 'users',
    'username' => 'email',
    'email' => 'email',
    'views' => true,
    'home' => '/',
    'routes' => [["title"=>"Главная", "route" => "home"]],
    'prefix' => '',
    'order' => 'order',
    'order_position' => 'ASC',
    'domain' => null,
    'limiters' => [
        'login' => null,
    ],
    'redirects' => [
        'login' => null,
        'logout' => null,
        'password-confirmation' => null,
        'register' => null,
        'email-verification' => null,
        'password-reset' => null,
    ],
    'features' => [
        Features::registration(),
        Features::resetPasswords(),
        Features::emailVerification(),
        Features::updateProfileInformation(),
        Features::updatePasswords(),
        Features::twoFactorAuthentication(),
    ],
    'requests' =>[
        'category_gallery' => [
            'title' => 'required',
            'description' => 'nullable',
            'image_url'=> 'string|nullable'
        ]
    ],
    'fillable' => [
        'category_gallery' => [
            'id','title','description','image_url'
        ]
    ]
];
